# Grundlagen für autonome Pressearbeit  
## Gliederung  
•	Einleitung  
•	Autonome Öffentlichkeitsarbeit: Immer ein Dilemma    
•	Dilemmata sind zum Aushalten da    
•	Was ist Öffentlichkeitsarbeit?    
•	Auswahl der richtigen Geschichten und Fakten (Strategie)  
•	Geschichten  
•	Fakten  
•	Journis als "Multiplikatoren"  
•	Zielgruppengerechte Kommunikation (Taktik)  
•	Was sind Zielgruppen  
•	Zielgruppen ansprechen  
•	Zentraler Begriff "Framing"  
•	Zentraler Begriff "Wording"  
•	Weiteres zur Ausdrucksweise  
•	möglichst effektive Verbreitung (Handwerkszeug)  
•	Autonome Öffentlichkeitsarbeit  
•	Hierarchien & Repräsentation (Grundsätze)  
•	Besetzungs-Accounts  
•	Interne Vermittlung & Offenheit für Feedback  
•	Fail forward: Fragend scheitern wir voran  
•	Formate
## Begriffslexikon
### Repräsentationsprinzip
Repräsentation ist wenn ein Mensch von viele spricht.

### PR
PR = Public Relations = Öffentlichkeitsarbeit

### Narrativ
Die Geschichte die wir erzählen wollen. Warum tun wir, was wir tun? Warum versucht die Polizei das zu verhindern? Welches Problem wollen wir lösen?

### Polarisierung
Wenn sich Meinungsunterschiede verschärfen, nur weil wir total vernünftige Sachen sagen. Beispiel: "Autos abschaffen? Also das ist jetzt aber ein total polarisierendes Statement."

### Zielgruppe
Die Menschen, die man ansprechen/erreichen will.

### Framing
Frame = Rahmen = Fenster auf die komplexe Realität.  
Wenn man ein Foto von einer Szene macht, ist das ja nie die ganze Wahrheit, sondern immer nur eine Perspektive.

### Wording
Das nützlichste Wort finden, um etwas zu beschreiben. Austauschbare Wörter haben oft drastisch unterschiedliche Bedeutungen:  
* Symbol / Protest / Widerstand
* spazieren / marschieren / ausrücken
* Klimawandel / Klimakrise / Klimakatastrophe

## Einleitung  
### Autonome Öffentlichkeitsarbeit: Immer ein Dilemma  
•	Hierarchiekritische Bewegung in Zusammenarbeit mit der "4. Säule des Staates"?!  
•	gleiche Aufteilung der Arbeit & Verantwortung funktioniert in der Praxis schlecht  
•	Repräsentationsprinzip, "Das Gesicht der Bewegung"  
•	"Pressesprecher:innen" bekommen (von der Presse) informelle Macht innerhalb der Bewegung  
•	Rolle nicht für alle zugänglich: mehr Sichtbarkeit für privilegierte Meinungen  
•	Macht korrumpiert: Anreiz und Gewohnheit, möglichst "vermittelbar" zu kommunizieren, weicht unbequeme Standpunkte auf  
•	"professionelle" Pressekontaktivistis mit informeller Machtposition argumentieren gegen unbequeme = schwer vermittelbare Aktionsformen  
•	Soweit irgendwelche Fragen? Bitte gerne jederzeit melden  
### Dilemmata sind zum Aushalten da  
•	"Be your own media" reicht nicht, um über unsere Bubble hinaus Meinungen zu beeinflussen  
•	Informelle Hierarchien gibt es sowieso - Arbeitsteilung macht sie sichtbar & kritisierbar  
•	d.h. im Fall von PR: Arbeitsteilung versetzt Pressekontaktivistis in die Lage, sich viel bewusster mit diesen Dilemmata zu konfrontieren  
•	im Fall von Waldbesetzungen/ zivilem Ungehorsam: Aktivist:innen in Polizeikontakt brauchen möglichst effektive PR für relative Sicherheit  
•	unsere politischen Gegner:innen nutzen die Mainstream-Medien sehr effektiv als Verstärker für ihre Meinungen - wir sollten ihnen die Bühne nicht überlassen  
•	Prozessorientierung: der bewusste Versuch, die Dilemmata zu überbrücken, könnte zu Lösungen der Probleme führen  
## Was ist Öffentlichkeitsarbeit?  
•	Hier geht es um klassische Öffentlichkeitsarbeit  
•	Basics von dem, was ihr auch von Unis oder NGO's lernen könnt  
•	Grundlage für die Suche nach einer "Autonomen Methode"  
•	Begriffsklärung (Frage in die Gruppe): Öffentlichkeitsarbeit, PR, Propaganda  
•	Ziele: Meinungen von Menschen beeinflussen, Sympathie steigern, Menschen an Bord holen, andere Akteure unter Druck setzen (z.B. Parteien, Gerichte, Polizei)  
  
### Auswahl der richtigen Geschichten und Fakten (Strategie)  
Geschichten  
•	meistens in kleinen Schritten  
•	meistens durch Emotionen (nicht gleich manipulativ: Meinungsbildung hat immer mit Identität zu tun = ist emotional)  
•	Geschichten erzählen: Begriffsklärung "Narrative"  
•	Standard-Diskussion: Welchen Platz hat Polizeigewalt in unseren Narrativen?  
Fakten  
•	Fakten auswählen tun alle immer: subjektive Realität (sonst Reizüberflutung)  
•	kurze Aufmerksamkeitsspanne von Medienkonsument:innen: Fakten verkürzen und Filtern  
•	2020er Jahre haben "alternative Fakten" und "Postfaktizismus", viel Polarisierung  
•	Balance zwischen die richtigen Fakten betonen und glaubwürdig wirken  
Journis als "Multiplikatoren"  
•	PR-Arbeit lebt von Zusammenarbeit mit Journis  
•	Begriffsklärung "Reichweite"  
•	Teil der Strategie: Mit denken, wie die denken  
•	viele glauben an das Ideal (oder die Illusion) der objektiven Berichterstattung  
•	Journis wollen selber gesehen haben (verifizieren) was sie berichten  
•	Wichtig für uns: Wahrnehmung/ Ruf der eigenen Glaubwürdigkeit bei den Journis  
•	Kontakte pflegen, einigermaßen seriös und freundlich auftreten  
•	Journis und kommerzielle Medien (z.B. Zeitungen, Fernsehsender) können nur berichten, was für ihr jeweiliges Publikum glaubwürdig klingt  
•	je unwahrscheinlicher eine Geschichte/ Tatsache aus Journi-Sicht scheint, desto besser muss sie belegt sein  
•	Redaktionskonferenzen meist konservativer als Journis  
•	gerade bei Fernsehsendern: Arbeitsteilung zwischen Vorort-Berichterstattung, redaktioneller Auswahl und Schnitt  
•	Enttäuschungen einplanen (Bsp. Galileo)  
### Zielgruppengerechte Kommunikation (Taktik)  
Was sind Zielgruppen  
•	"breite Masse" gibt es nicht  
•	Gesellschaft besteht aus verschiedenen Gruppen mit unterschiedlichen Sprachen, Erfahrungshorizonten, Wertesystemen; Übergänge sind fließend  
•	Propaganda kann nie alle Menschen gleich gut erreichen  
•	deshalb bewusste Entscheidung für eine oder mehrere Zielgruppen  
•	dabei sogut es geht auch andere Zielgruppen noch mit ansprechen  
•	steile These: wer nicht aktiv über seine Zielgruppen reflektiert, wird automatisch immer nur für die eigene Echokammer schreiben  
•	Besonderheit bei Pressetexten: Zielgruppe sind immer auch Journis, weil die filtern, was "relevant" ist  
Zielgruppen ansprechen  
•	Menschen haben begrenzten Realitätshorizont  
•	Was zu weit von ihren Denkgewohnheiten abweicht, werden sie schnell als ganz bestimmt falsch abtun.  
•	"Menschen da abholen, wo sie stehen"  
•	Als Beispiel: 2 Aussagen, die zwar (meiner Meinung nach) wahr sind, aber nicht hilfreich, zu sagen:  
•	Thema Klimawandel: "Das einzige, was wir sicher wissen ist: Die Welt, wie wir sie kennen, haben wir höchstwahrscheinlich schon zerstört."  
•	(Triggerwarnung) Thema Polizeigewalt: "In Deutschen Polizeizellen wird immer wieder gefoltert und gemordert."  
•	beide Aussagen würden, so ungefiltert, die meisten durchschnittlichen weiß-deutschen Zeitungsleserin:innen effektiv null erreichen, weil für sie nicht vorstellbar und zu überfordernd  
•	Wieviel nützt ungefilterte Wahrheit in welchen Fällen?  
•	Wie sehr können wir welche Zielgruppe in welcher Situation zum Umdenken herausfordern?  
•	Welche Nebenwirkungen kann eine bestimmte Aussage haben, wenn sie die falsche Zielgruppe erreicht? Lohnt sich das? Wie gegensteuern?  
•	Spricht eine konkrete Aktion/ Situation für sich selbst? Was müssen wir erklären?  
•	Übung: Richtig viel mit Menschen aus unterschiedlichen soziologischen und ideologischen Hintergründen reden. Tellerrand wegsprengen.  
Zentraler Begriff "Framing"  
•	eine andere Metapher für diesen Realitätshorizont und einen bewussten Umgang damit  
•	Frame = Rahmen = Fenster auf die komplexe Realität  
•	PR-Arbeit will passende "Rahmen" an den bestehenden Realitätshorizont dran basteln, um ihn in die gewünschte Richtung zu erweitern  
•	Gruppenabsprachen zu Framing-Strategie sind wichtig  
•	auch zentraler Teil der Bündnisarbeit  
•	Beispielsatz: "Wollen wir die Aktion als Vergeltungsmaßnahme oder als Verzweiflungstat framen?"  
Zentraler Begriff "Wording"  
•	Propaganda = Krieg der Worte  
•	austauschbare Wörter haben oft drastisch unterschiedliche Bedeutungen  
•	Beispiele:  
•	Symbol / Protest / Widerstand  
•	spazieren / marschieren / ausrücken  
•	Aktion / Angriff / Selbstverteidigung  
•	Erschließung / Zerstörung / Verwertung (gerade hier: je nach Zielgruppe)  
•	Militanz / Verzweiflung / Entschlossenheit  
•	Klimawandel / Klimakrise / Klimakatastrophe  
•	genau hinschauen: was kommt beim Publikum an? Tw. subtile Unterschiede  
•	Gruppenabsprachen zu Wording-Strategie sind wichtig  
•	auch zentraler Teil der Bündnisarbeit  
•	Beispielsatz: "Ich würde für ein gemeinsames Wording und die Wiedererkennbarkeit ein Paar Schlüsselbegriffe herausheben."  
#### Weiteres zur Ausdrucksweise  
•	Szene-Sprech und Uni-Blabla erkennen, abschalten lernen und konsequent wegkorrigieren, wenn dein eigenes Umfeld nicht deine Zielgruppe ist.  
•	formelle Ausdrucksweise beherrschen, aber nicht übertreiben  
•	Zeitung lesen hilft, um Gefühl für deren Tonfall zu bekommen  
•	unterschiedliche Zielgruppen und Medien brauchen unterschiedlichen Sprachstil  
### Möglichst effektive Verbreitung (Handwerkszeug)  
#### Was ist Pressearbeit? Verschiedene Aufgaben:  
•	Sprecher:innen/ "Pressesprechende" (können vorher geschriebene Narrative nutzen)  
•	Fotos & Videos aufnehmen & schneiden  
•	Social Media  
•	Pressehandy / Kontakt-Kanäle  
•	Pressemitteilungen  
•	Pressespiegel  
•	Verteiler pflegen  
•	Backoffice-Infrastruktur aufbauen  
•	Kontakt zu Aktivist:innen, z.B. Feedback einholen, Narrative dezentral entscheiden  
#### Wie in die Presse kommen?  
•	formale Bedingungen  
•	Aktualität & Relevanz  
•	Verteiler  
#### Presseausweise als Passierschein  
•	DVPJ funktioniert: 153€  
•	theoretisch Nachweis von Pressetätigkeit nötig, seit Corona ist das einfacher. man kann zB einfach "reiche ich nach" anklicken  
•	Begleitpersonen werden idR durchgelassen (Assistenz)  
#### Pressekonferenzen  
•	In die Besetzung fragen, welche Narrative sie sehen wollen/Strategie einfließen lassen  
•	Sprecher:innen finden  
•	Presse-Einladung (min. 2 Tage vorher, bei weniger aktuellen Themen lieber mehr)  
•	Wortbeiträge der Sprecher:innen abklären, am besten in einem Pad  
•	Biertische & Bänke  
•	Akkubox + Mikro  
#### Ticker  
•	Kanäle, die sich anbieten: Telegram, Twitter, Signal, Mastodon.  
•	Faktencheck vor dem posten? In der Praxis nicht immer gut machbar. Aber auch nicht unbedingt nötig.  
•	Obwohl der Action-Point am Ende alles getickert hat was ihnen am Telefon gesagt wurde, wurde der Account zumindest von der Presse sehr ernst genommen.  
## Autonome Öffentlichkeitsarbeit  
### Hierarchien & Repräsentation (Grundsätze)  
Hierarchien:  
•	entstehen durch Wissen, Selbstsicherheit, Kontakte, Annahmen darüber  
•	Aufgaben rotieren hilft  
•	traut euch, neue Aufgaben zu übernehmen  
•	Buddy-Prinzip: Menschen aktiv einarbeiten  
•	Skillshare-Tabelle: was gibt es zu können? Das meiste ist in einer Stunde vermittelbar  
•	Fehlertoleranz ist wichtig für die Revolution - sowohl in Bezug auf perfekte Pressearbeit, als auch in Sachen Hierarchiefreiheit  
Presse will "Das Gesicht der Bewegung" zeigen. Wir nicht.  
•	Andere Stimmen werden nicht gehört  
•	Presse peilt oft nicht, dass wir keine Organisation sind (bzw. unterschätzen die Fähigkeit ihres Publikums, das zu verstehen)  
•	zentrale Gesichter können leichter unter Druck gesetzt werden (Distanzierungen)  
•	Macht korrumpiert. Politische Gegner bieten Gesichtern Deals an  
•	ist nicht unbedingt falsch, aber es können nicht alle mitverhandeln  
•	Verantwortung spült weich. Falsche Vorsicht gegenüber der öffentlichen Meinung  
•	Fehlverhalten von "Führungspersonal" kann leichter rausgepickt und unfair kritisiert werden werden  
•	Gesichter machen (Meinungs-)Diversität der Bewegung unsichtbar  
Repräsentationsproblem  
•	Jede unserer Meinungsäußerungen wird auf die ganze Bewegung projiziert  
•	Betonen, dass man immer nur für sich selbst spricht. Kommt aber meistens nicht an.  
•	Presse und deren Publikum interessieren sich großteils nicht für unsere Diversität.  
•	Wer Pressearbeit macht (ob ausnahmsweise oder hauptsächlich) kommt nicht an der Verantwortung vorbei, bestmöglich zu repräsentieren.  
•	Repräsentationsparadox: Es ist nicht möglich, für eine diverse Gruppe zu sprechen, also müssen wir es tun.  
### Besetzungs-Accounts  
Skala-Übung  
•	Kann man öffentlich auf dem Hauptaccount politische Nacktbilder posten?  
•	Wenn jemand anders sich gestört fühlt, kann die Person das dann löschen?  
•	Wenn jemand anders sich gestört fühlt, kann die Person eine Richtigstellung/Zusatz da drunter kommentieren?  
•	Kann man Kritik an Bündnispartner:innen vom Hauptaccount aus äußern?  
•	Kann man solche Kritik dann einfach löschen?  
•	Kann man solche Kritik klarstellen?  
pragmatische Probleme:  
•	Zugriff von vielen Endgeräten kann zu Sperrungen führen  
•	Ein gewisser Standard auf Social Media ist nötig für Reichweite  
Umgang des Mediahub bei der Danni-Räumung  
•	Ein Mobilisierungs-Account, der von einem Team gepflegt wird, das sich im Konsens abspricht  
•	Dezentrale Barrio-Accounts  
•	Ein Ticker-Account für aktuelle Geschehnisse  
•	was fehlte: Ein zentraler Haupt-Account, mit Zugang für alle  
•	Konflikte vermeiden: Früh vorbereiten, dass die unterschiedlichen Accounts mit ihren unterschiedlichen Aufgaben wahrgenommen und gewertschätzt werden  
•	Früh vorbereiten: Chat-Gruppen, um sich absprechen zu können.  
### Diversity of Tactics  
•	Klimakrise ist existenzielle Bedrohung; verschiedene Menschen finden verschiedene Arten, damit umzugehen.  
•	historisch über die Suffragetten/Haymarket Riots: andere Bewegungen haben ihre Punkte auch nur durch Militanz durchbekommen.  
•	Super, dass sie mit mir über Gewalt sprechen wollen, die Räumungspanzer und Bagger sind ja auch richtig schlimm.  
•	Immer auf letzte Polizeigewalt hinweisen, wenn wir darauf angesprochen werden (aber nicht permanent thematisieren, hält Leute fern)  
•	David gegen Goliath: riesiger Polizeistaat, kleine Gruppe von Aktivist:innen  
•	Dominanzprinzip: Diskussion auf eine andere Ebene schieben  
•	Whataboutism kann nützlich sein fürs Dominanzprinzip, bringt aber inhaltlich nicht weiter  
•	Mit politischer Ignoranz argumentieren: wir werden ja nicht gehört  
•	Nicht von Strategie reden; es gibt ja keine Strategie  
### Interne Vermittlung & Offenheit für Feedback  
•	Weil Pressearbeit eine repräsentative Funktion übernimmt, muss sie kritisierbar sein  
•	Wege um offen für Beiträge aus der Bewegung zu sein:  
•	Wöchentliche Treffen bei denen man nach Feedback fragt sind eine gute Idee  
•	sonst ist eine tägliche Sprechstunde auch eine gute Idee  
•	proaktiv vermitteln, wie man Teil des Teams werden kann  
•	regelmäßig Skillshares anbieten, auch wenn nicht immer jemand kommt  
•	Manchmal kritisieren Leute ohne einen Verbesserungsvorschlag zu liefern.  
•	Das tut weh und ist nicht konstruktiv, aber repräsentieren ist nun mal eig falsch  
•	Je mehr man proaktiv offen ist und Leute einbindet, desto weniger Konflikte  
### Fail forward: Fragend scheitern wir voran  
•	Da gehört viel dazu, und das klappt natürlich alles nicht immer  
•	Oft kann man nicht klar einschätzen, ob eine Kampagne/Aktion gut ankam oder nicht  
•	Feedback aus der Bewegung ist das eine, öffentliche Meinung das andere  
•	Man muss aufpassen, sich nicht der öffentlichen Meinung zu beugen  
•	Die Mehrheit ist nicht auf unserer Seite, die Mehrheit ist okay mit der Normalität  
•	Der Einfluss unserer Arbeit auf das Medienecho ist sehr begrenzt  
•	Also macht euch nicht fertig wenns nicht so klappt wie ihr gerne hättet.  
•	Politischer Kampf ist kein Sprint, sondern ein Marathon  
  
  
